//
//  ViewController.swift
//  Project25
//
//  Created by Роман Хоменко on 26.06.2022.
//

import MultipeerConnectivity
import UIKit

class ViewController: UICollectionViewController {
    // Properties
    var images = [UIImage]()
    
    var peerID: MCPeerID = MCPeerID(displayName: UIDevice.current.name)
    var mcSession: MCSession?
    var mcAdvertiserAssistant: MCNearbyServiceAdvertiser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupToolBar()
        initEmptySession()
    }
}

// MARK: Setup NavigationController and its toolBar
extension ViewController {
    func setupNavigationBar() {
        title = "Selfie Share"
        
//        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera,
//                                                            target: self,
//                                                            action: #selector(importPicture))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "",
                                                            image: Image.add,
                                                            primaryAction: nil,
                                                            menu: showActionAddButton())
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: nil,
                                                           image: Image.host,
                                                           primaryAction: nil,
                                                           menu: showConnectionPrompt())
    }
}

// MARK: - Configure toolBar
extension ViewController {
    // First setup toolBar
    func setupToolBar() {
        DispatchQueue.main.async {
            self.navigationController?.isToolbarHidden = false
            let connectedDevices = UIBarButtonItem(title: "",
                                                   image: Image.connected,
                                                   primaryAction: nil,
                                                   menu: self.showConnectedDevices())
            
            self.setToolbarItems([connectedDevices], animated: true)
        }
    }
    
    func showConnectionPrompt() -> UIMenu {
        let host = UIAction(title: "Host a session",
                            image: Image.host) { [weak self] action in
            self?.startHosting(action: action)
        }
        let join = UIAction(title: "Join a session",
                            image: Image.join) { [weak self] action in
            self?.joinSession(action: action)
        }
        let stop = UIAction(title: "Stop session",
                            image: Image.stop,
                            attributes: .destructive) { [weak self] action in
            self?.stopSession(action: action)
        }
        let cancel = UIAction(title: "Cancel") { _ in }
        
        let menu = UIMenu(title: "Multipeer Connectivity",
                          image: nil,
                          identifier: nil,
                          options: [],
                          children: [host, join, stop, cancel])
        
        return menu
    }
    
    func showActionAddButton() -> UIMenu {
        let camera = UIAction(title: "Photos from Camera",
                            image: Image.camera) { [weak self] action in
            self?.importPicture(from: .library)
        }
        let library = UIAction(title: "Photots from Library",
                               image: Image.photoLibrary) { [weak self] action in
//            self?.joinSession(action: action)
        }
        let text = UIAction(title: "Text Message",
                            image: Image.messageBubble) { [weak self] action in
//            self?.stopSession(action: action)
        }
        let cancel = UIAction(title: "Cancel") { _ in }
        
        var actions: [UIAction] = []
        #if targetEnvironment(simulator)
        actions = [library, text, cancel]
        #else
        actions = [camera, library, text, cancel]
        #endif
        
        let menu = UIMenu(title: "",
                          image: nil,
                          identifier: nil,
                          options: [],
                          children: [camera, library, text, cancel])
        
        return menu
    }
}

// MARK: - @objc methods
extension ViewController {
    func importPicture(from: PictureFrom) {
        if from == .library {
            
        } else {
            let picker = UIImagePickerController()
            picker.allowsEditing = true
            picker.delegate = self
            present(picker, animated: true)
        }
    }
    
    func showConnectedDevices() -> UIMenu {
        let menuItems: [UIAction] = mcSession?.connectedPeers.map({ connectedDevice in

            print(connectedDevice.displayName)

            return UIAction(title: connectedDevice.displayName,
                            image: Image.iPhone,
                            handler: { (_) in })
        }) ?? []

        print(menuItems.count)

        return UIMenu(title: "Connected devices",
                      image: nil,
                      identifier: nil,
                      options: [],
                      children: menuItems)
    }
}
// MARK: - CollectionView methods
extension ViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageView", for: indexPath)
        cell.contentView.layer.cornerRadius = 8
        
        if let imageView = cell.viewWithTag(1000) as? UIImageView {
            imageView.image = images[indexPath.item]
        }
        
        return cell
    }
}

// MARK: - Confirmation to NavigationDelegate and UIImagePickerControllerDelegate proocols
extension ViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else { return }
        dismiss(animated: true)
        
        images.insert(image, at: 0)
        collectionView.reloadData()
        
        guard let mcSession = mcSession else { return }
        
        if mcSession.connectedPeers.count > 0 {
            if let imageData = image.pngData() {
                do {
                    try mcSession.send(imageData,
                                       toPeers: mcSession.connectedPeers,
                                       with: .reliable)
                } catch let error as NSError {
                    let alert = UIAlertController(title: "Send error",
                                                  message: error.localizedDescription,
                                                  preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK",
                                                  style: .default))
                    present(alert, animated: true)
                }
            }
        }
    }
}

// MARK: - MultipeerConnectivity helper methods and delegates
extension ViewController: MCNearbyServiceAdvertiserDelegate {
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        let ac = UIAlertController(title: title,
                                   message: "\(peerID.displayName) wants to connect",
                                   preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Allow",
                                   style: .default) { [weak self] _ in
            invitationHandler(true, self?.mcSession)
        })
        ac.addAction(UIAlertAction(title: "Decline",
                                   style: .cancel) { _ in
            invitationHandler(false, nil)
        })

        present(ac, animated: true)
    }
    
    
    func initEmptySession() {
        mcSession = MCSession(peer: peerID,
                              securityIdentity: nil,
                              encryptionPreference: .required)
        mcSession?.delegate = self
        
    }
    
    func startHosting(action: UIAction) {
        guard mcSession != nil else { return }
        mcAdvertiserAssistant = MCNearbyServiceAdvertiser(peer: peerID,
                                                          discoveryInfo: nil,
                                                          serviceType: MCConnectivity.serviceID)
        mcAdvertiserAssistant?.delegate = self
        mcAdvertiserAssistant?.startAdvertisingPeer()
    }
    
    func joinSession(action: UIAction) {
        guard let mcSession = mcSession else {
            return }
        let mcBrowser = MCBrowserViewController(serviceType: MCConnectivity.serviceID,
                                                session: mcSession)
        mcBrowser.delegate = self
        present(mcBrowser, animated: true)
    }
    
    func stopSession(action: UIAction) {
        guard let mcSession = mcSession else {
            return
        }
        mcAdvertiserAssistant?.stopAdvertisingPeer()
        mcSession.disconnect()
    }
    
    func alertWithDisconnectedDevice(with name: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Disconnected device",
                                          message: "\(name)",
                                          preferredStyle: .alert)
            let action = UIAlertAction(title: "OK",
                                       style: .default)
            alert.addAction(action)
            
            self.present(alert, animated: true)
        }
    }
}


// MARK: - MCBrowserVCDelegate and MCSessionDelegate methods
extension ViewController: MCSessionDelegate, MCBrowserViewControllerDelegate {
    // Catch data being received
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        DispatchQueue.main.async { [weak self] in
            if let image = UIImage(data: data) {
                self?.images.insert(image, at: 0)
                self?.collectionView.reloadData()
            }
        }
    }
    
    // Conecting's inspector method
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case .connected:
            print("Connected: \(peerID.displayName)")
            setupToolBar()
        case .connecting:
            print("Connecting: \(peerID.displayName)")
        case .notConnected:
            print("Not Connecting: \(peerID.displayName)")
            // work only in main thread
            alertWithDisconnectedDevice(with: peerID.displayName)
        @unknown default:
            print("Unknown state received: \(peerID.displayName)")
        }
    }
    
    // Dismiss browserVC methods
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }
    
    // Unused reequired methods
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
    }
}
