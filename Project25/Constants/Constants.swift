//
//  Constants.swift
//  Project25
//
//  Created by Роман Хоменко on 05.07.2022.
//

import UIKit

enum Image {
    static let add = UIImage(systemName: "plus")
    static let connection = UIImage(systemName: "dot.radiowaves.left.and.right")
    static let host = UIImage(systemName: "antenna.radiowaves.left.and.right")
    static let join = UIImage(systemName: "personalhotspot")
    static let stop = UIImage(systemName: "nosign")
    static let camera = UIImage(systemName: "camera")
    static let photoLibrary = UIImage(systemName: "photo.on.rectangle.angled")
    static let messageBubble = UIImage(systemName: "message")
    static let connected = UIImage(systemName: "network")
    static let iPhone = UIImage(systemName: "iphone")
}


enum MCConnectivity {
    static let serviceID = "prod-project25"
}

enum PictureFrom {
    case library
    case photo
}
